//
//  ViewController.swift
//  Dialog
//
//  Created by Artem Volkov on 07/12/2019.
//  Copyright © 2019 Artem Work. All rights reserved.
//

import Alamofire

class ViewController: UIViewController {
    @IBOutlet weak var answerPicker: UIPickerView?
    @IBOutlet weak var sendButton: UIButton?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?

    private var embeddedViewController: DialogViewController?

    @IBAction func actionSend(_ sender: UIButton) {
        guard let embeddedViewController = embeddedViewController else {
            print("Error: embeddedViewController not found!")
            return
        }
        embeddedViewController.send()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? DialogViewController,
                    segue.identifier == "EmbedSegue" {
            self.embeddedViewController = vc
            vc.activityIndicator = activityIndicator
            vc.sendButton = sendButton
            vc.answerPicker = answerPicker
            vc.answerPicker?.dataSource = vc
            vc.answerPicker?.delegate = vc
        }
    }
}

