//
//  NetworkService.swift
//  Dialog
//
//  Created by Artem Volkov on 07/12/2019.
//  Copyright © 2019 Artem Work. All rights reserved.
//

import Alamofire

class NetworkService {
    static let sharedInstance = NetworkService()

    private static let serverAddress = "185.65.202.192"
    private static let port = 8080
    private static let scheme = "http"

    static let language = Locale.current.languageCode ?? "ru"

    //http://185.65.202.192:8080/request/test
    static let historyUrl = "\(scheme)://185.65.202.192:\(port)/history"
    static let dialogInitUrl = "\(scheme)://185.65.202.192:\(port)/request/init"
    static let dialogItemUrl = "\(scheme)://185.65.202.192:\(port)/request"



    func history(completion:@escaping (Result<HistoryResponse>) -> Void)
    {
        let url = Self.historyUrl
        let historyRequest = HistoryRequest()
        post(url: url, request: historyRequest) { (result: Result<HistoryResponse>) in
            completion(result)
        }
    }

    func dialogInit(completion:@escaping (Result<DialogItemResponse>) -> Void)
    {
        let url = Self.dialogInitUrl
        post(url: url, request: HistoryRequest()) { (result: Result<DialogItemResponse>) in
            completion(result)
        }
    }

    func dialogItem(request: DialogItemRequest, completion:@escaping (Result<DialogItemResponse>) -> Void)
    {
        let url = Self.dialogItemUrl
        post(url: url, request: request) { (result: Result<DialogItemResponse>) in
            completion(result)
        }
    }

}




