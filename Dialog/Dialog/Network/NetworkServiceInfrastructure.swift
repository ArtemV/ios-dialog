//
//  NetworkServiceInfrastructure.swift
//  Dialog
//
//  Created by Artem Volkov on 07/12/2019.
//  Copyright © 2019 Artem Work. All rights reserved.
//

import Alamofire

extension NetworkService {
    func post<RBRequest: Encodable, RBResponce: Decodable>(url urlString: String,
                                                           request: RBRequest,
                                                           completion:@escaping (Result<RBResponce>) -> Void)
    {
        print("Sent request: \(urlString)     \(request)")
        let encoder = JSONEncoder()
        guard let jsonData = try? encoder.encode(request) else {
            print("Encoding failed: \(request)")
            completion(.failure(NSError(domain:"Network error: encoding failed", code:0, userInfo:nil)))
            return
        }

        guard let url = URL(string: urlString) else {
            print("URL creation failed: \(urlString)")
            completion(.failure(NSError(domain:"Network error: URL creation failed", code:0, userInfo:nil)))
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData

        Alamofire
            .request(request)
            .responseJSON { (response) -> Void in
            print("Received responce for: \(urlString)")
            print(response)
            if response.result.isSuccess {
                guard let data = response.data else {
                    completion(.failure(NSError(domain:"Network error: no data in responce.", code:0, userInfo:nil)))
                    return
                }

                do {
                    let decoder = JSONDecoder()
                    print(String(data: data, encoding: .utf8)!)
                    let objectFromResponce = try decoder.decode(RBResponce.self, from: data)
                    completion(.success(objectFromResponce))
                } catch let error {
                    print(error)
                    completion(.failure(error))
                }
            } else {
                if let error: Error = response.result.error {
                    completion(.failure(error))
                }
            }
        }
    }
}
