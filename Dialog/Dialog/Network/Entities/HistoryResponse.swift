//
//  HistoryResponse.swift
//  Dialog
//
//  Created by Artem Volkov on 07/12/2019.
//  Copyright © 2019 Artem Work. All rights reserved.
//

import Foundation

typealias HistoryResponse = [History.Record]

struct History: Decodable {

    struct Record: Decodable {
        let messageId: String
        let messageText: String
        let type: String
        let timestamp: Int

        let chatElementId: String
        let userAnswerText: String

        enum CodingKeys: String, CodingKey {
            case messageId
            case messageText
            case type

            case chatElementId
            case userAnswerText
            case timestamp = "messageReceivedAt"
        }
    }
}

extension History.Record {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        guard let timestampInt = Int(try values.decode(String.self, forKey: .timestamp)) else {
            throw "Network error: timestampInt failed"
        }
        timestamp = timestampInt
        messageId = try values.decode(String.self, forKey: .messageId)
        messageText = try values.decode(String.self, forKey: .messageText)
        type = try values.decode(String.self, forKey: .type)
        chatElementId = try values.decode(String.self, forKey: .chatElementId)
        userAnswerText = try values.decode(String.self, forKey: .userAnswerText)
    }
}
