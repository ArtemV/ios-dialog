//
//  DialogItemResponse.swift
//  Dialog
//
//  Created by Artem Volkov on 07/12/2019.
//  Copyright © 2019 Artem Work. All rights reserved.
//

import Foundation

struct DialogItemResponse: Decodable {
    let options: [Option]
    let message: Message

    enum CodingKeys: String, CodingKey {
        case options = "elements"
        case message
    }

    struct Message: Decodable {
        let id: String
        let text: String
        let type: String
//        let timestamp: String?

        enum CodingKeys: String, CodingKey {
            case id
            case text
            case type
//            case timestamp = "messageReceivedAt"
        }
    }

    struct Option: Decodable {
        let id: String
        let text: String
        let type: String

        enum CodingKeys: String, CodingKey {
            case id
            case text
            case type // = "typeOfElement"
        }
    }
}

//{
//    "message": {
//        "type": "MESSAGE",
//        "id": "init",
//        "text": "",
//        "linksID": [
//            "init"
//        ]
//    },
//    "elements": [
//        {
//            "type": "BUTTON",
//            "id": "RU",
//            "text": "Русский",
//            "linksID": [
//                "init"
//            ]
//        },
//        {
//            "type": "BUTTON",
//            "id": "EN",
//            "text": "English",
//            "linksID": [
//                "init"
//            ]
//        }
//    ]
//}


