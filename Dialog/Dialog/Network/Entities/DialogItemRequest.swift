//
//  DialogItemRequest.swift
//  Dialog
//
//  Created by Artem Volkov on 07/12/2019.
//  Copyright © 2019 Artem Work. All rights reserved.
//

import Foundation

// reference
// { “language”: “ru” , “chatElementId”: “card”, “messageId”: “productType” }

struct DialogItemRequest: Encodable {
    let language = NetworkService.language.capitalizingFirstLetter()
    let chatElementId: String
    let messageId: String
}
