//
//  HistoryRequest.swift
//  Dialog
//
//  Created by Artem Volkov on 07/12/2019.
//  Copyright © 2019 Artem Work. All rights reserved.
//

import Foundation

struct HistoryRequest: Encodable {
    let language = NetworkService.language.capitalizingFirstLetter()
}
