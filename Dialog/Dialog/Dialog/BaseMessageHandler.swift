//
//  BaseMessageHandler.swift
//  Dialog
//
//  Created by Artem Volkov on 07/12/2019.
//  Copyright © 2019 Artem Work. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

final class GenericMessageHandler<ViewModel: MessageViewModelProtocol>: BaseMessageInteractionHandlerProtocol {

    init() {
    }

    func userDidTapOnFailIcon(viewModel: ViewModel, failIconView: UIView) {
    }

    func userDidTapOnAvatar(viewModel: ViewModel) {
    }

    func userDidTapOnBubble(viewModel: ViewModel) {
    }

    func userDidBeginLongPressOnBubble(viewModel: ViewModel) {
    }

    func userDidEndLongPressOnBubble(viewModel: ViewModel) {
    }

    func userDidSelectMessage(viewModel: ViewModel) {
    }

    func userDidDeselectMessage(viewModel: ViewModel) {
    }
}

class BaseMessageHandler: BaseMessageInteractionHandlerProtocol {

    init() {
    }
    func userDidTapOnFailIcon(viewModel: MessageViewModelProtocol, failIconView: UIView) {
        print("userDidTapOnFailIcon")
    }

    func userDidTapOnAvatar(viewModel: MessageViewModelProtocol) {
        print("userDidTapOnAvatar")
    }

    func userDidTapOnBubble(viewModel: MessageViewModelProtocol) {
        print("userDidTapOnBubble")
    }

    func userDidBeginLongPressOnBubble(viewModel: MessageViewModelProtocol) {
        print("userDidBeginLongPressOnBubble")
    }

    func userDidEndLongPressOnBubble(viewModel: MessageViewModelProtocol) {
        print("userDidEndLongPressOnBubble")
    }

    func userDidSelectMessage(viewModel: MessageViewModelProtocol) {
        print("userDidSelectMessage")
    }

    func userDidDeselectMessage(viewModel: MessageViewModelProtocol) {
        print("userDidDeselectMessage")
    }
}
