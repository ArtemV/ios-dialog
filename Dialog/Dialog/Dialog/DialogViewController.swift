//
//  DialogViewController.swift
//  Dialog
//
//  Created by Artem Volkov on 07/12/2019.
//  Copyright © 2019 Artem Work. All rights reserved.
//

import UIKit
import MessageKit
import InputBarAccessoryView
import Alamofire

class DialogViewController: MessagesViewController, MessagesDataSource {

    let network  = NetworkService.sharedInstance
    let displayDelegate = DialogMessagesDisplayDelegate()
    let layoutDelegate = DialogMessagesLayoutDelegate()

    weak var answerPicker: UIPickerView?
    weak var sendButton: UIButton?
    weak var activityIndicator: UIActivityIndicatorView?

    var lastDialogItem: DialogItemResponse?
    var answerPickerData: [DialogItemResponse.Option] = []
    func send() {
        guard let row = answerPicker?.selectedRow(inComponent: 0) else {
            assertionFailure()
            return
        }
        guard let lastDialogItem = lastDialogItem else {
            print("Warning: no last message.")
            return
        }

        setupViews(enabled: false)
        let selectedOption = answerPickerData[row]
        let userAnswer = MessageItem(text: selectedOption.text,
                                     user: self.currentSender(),
                                     messageId: UUID().uuidString,
                                     date: Date())
        self.insertMessage(userAnswer)
        self.messagesCollectionView.reloadData()
        self.messagesCollectionView.scrollToBottom()


        //{"language": "ru", "messageId": "card_productConfirm", "chatElementId":"card_productConfirmYes"}
//        let request = DialogItemRequest(chatElementId: "card_productConfirmYes",
//                                        messageId: "card_productConfirm")
        let request = DialogItemRequest(chatElementId: selectedOption.id,
                                        messageId: lastDialogItem.message.id)
        network.dialogItem(request: request) {[weak self] (result: Result<DialogItemResponse>) in
            guard let self = self else {
                return
            }
            switch result {
            case .success(let dialogItem):
                self.insertServerMessages(dialogItem)
                self.messagesCollectionView.scrollToBottom(animated: true)
                self.lastDialogItem = dialogItem
                self.answerPickerData = dialogItem.options
                self.answerPicker?.reloadAllComponents()
            case .failure(let error):
                print(error)
                let alert = UIAlertController(title: "Сервер не отвечает",
                                              message: "Пожалуйста, проверьте настройки интернет соединения и повторите попытку",
                                              preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            self.setupViews(enabled: true)
        }
    }

    private func setupViews(enabled: Bool) {

        let views = [sendButton ?? UIView(), answerPicker ?? UIView()]
        for view in views {
            view.isUserInteractionEnabled = enabled
        }
        if enabled {
            activityIndicator?.stopAnimating()
        } else {
            activityIndicator?.startAnimating()
        }
    }

    var messageList: [MessageType] = []

    let refreshControl = UIRefreshControl()

    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureMessageCollectionView()
        configureMessageInputBar()
        loadFirstMessages()
        title = "MessageKit"
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func initDialog() {
        network.dialogInit {[weak self] (result: Result<DialogItemResponse>) in
            guard let self = self else {
                return
            }
            switch result {
            case .success(let dialogItem):
                self.insertServerMessages(dialogItem)
                self.messagesCollectionView.scrollToBottom(animated: true)
                self.lastDialogItem = dialogItem
                self.answerPickerData = dialogItem.options
                self.answerPicker?.reloadAllComponents()
            case .failure(let error):
                print(error)
                let alert = UIAlertController(title: "Сервер не отвечает",
                                              message: "Не удалось запустить диалоговую систему, проверьте настройки интернет соединения и ПЕРЕЗАПУСТИТЕ приложение",
                                              preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            self.setupViews(enabled: true)
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    func serverAsSender() -> SenderType {
        return Sender(id: "example_unique_server_user_id", displayName: "Система Диалог")
    }

    func loadFirstMessages() {
        setupViews(enabled: false)
        network.history {[weak self] (result: Result<HistoryResponse>) in
            guard let self = self else {
                return
            }
            switch result {
            case .success(let historyResponse):
                let messages = historyResponse.map { (record: History.Record) -> [MessageItem] in
                    return [MessageItem(text: record.messageText,
                                       user: self.serverAsSender(),
                                       messageId: record.messageId,
                                       date: Date(timeIntervalSince1970: TimeInterval(record.timestamp))),
                            MessageItem(text: record.userAnswerText,
                                        user: self.currentSender(),
                                        messageId: record.messageId + record.chatElementId,
                                        date: Date(timeIntervalSince1970: TimeInterval(record.timestamp)))
                            ]
                }
                let flatMessages = messages.flatMap{ $0 }
                self.messageList = flatMessages
                self.messagesCollectionView.reloadData()
                self.messagesCollectionView.scrollToBottom()
            case .failure(let error):
                print(error)
            }
            self.initDialog()
        }

//        self.messageList = [MessageItem(text: "helloo!!",
//                                        user: self.currentSender(),
//                                        messageId: UUID().uuidString,
//                                        date: Date())]
//        self.messagesCollectionView.reloadData()
//        self.messagesCollectionView.scrollToBottom()
    }

    func configureMessageCollectionView() {
        messagesCollectionView.backgroundColor = .clear
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messageCellDelegate = self

        scrollsToBottomOnKeyboardBeginsEditing = true // default false
        maintainPositionOnKeyboardFrameChanged = true // default false

        messagesCollectionView.messagesLayoutDelegate = layoutDelegate
        displayDelegate.dataSource = self
        messagesCollectionView.messagesDisplayDelegate = displayDelegate
    }

    func configureMessageInputBar() {
        messageInputBar.isHidden = true
        messageInputBar.delegate = self
        messageInputBar.inputTextView.tintColor = .red
        messageInputBar.sendButton.setTitleColor(.red, for: .normal)
        messageInputBar.sendButton.setTitleColor(
            UIColor.red.withAlphaComponent(0.3),
            for: .highlighted
        )
    }

    // MARK: - Helpers

    func insertMessage(_ message: MessageType) {
        messageList.append(message)
        // Reload last section to update header/footer labels and insert a new one
        messagesCollectionView.performBatchUpdates({
            messagesCollectionView.insertSections([messageList.count - 1])
            if messageList.count >= 2 {
                messagesCollectionView.reloadSections([messageList.count - 2])
            }
        }, completion: { [weak self] _ in
            if self?.isLastSectionVisible() == true {
                self?.messagesCollectionView.scrollToBottom(animated: true)
            }
        })
    }

    func isLastSectionVisible() -> Bool {

        guard !messageList.isEmpty else { return false }

        let lastIndexPath = IndexPath(item: 0, section: messageList.count - 1)

        return messagesCollectionView.indexPathsForVisibleItems.contains(lastIndexPath)
    }

    // MARK: - MessagesDataSource

    func currentSender() -> SenderType {
        return Sender(id: "example_unique_user_id", displayName: "Иванов И.И.")
    }

    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messageList.count
    }

    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messageList[indexPath.section]
    }

    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if indexPath.section % 3 == 0 {
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate),
                                      attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10),
                                                   NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
        return nil
    }

    func cellBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        return NSAttributedString(string: "Read",
                                  attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10),
                                               NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }

    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        return NSAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])
    }

    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {

        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }

}



// MARK: - MessageLabelDelegate

extension DialogViewController: MessageLabelDelegate {

    func didSelectAddress(_ addressComponents: [String: String]) {
        print("Address Selected: \(addressComponents)")
    }

    func didSelectDate(_ date: Date) {
        print("Date Selected: \(date)")
    }

    func didSelectPhoneNumber(_ phoneNumber: String) {
        print("Phone Number Selected: \(phoneNumber)")
    }

    func didSelectURL(_ url: URL) {
        print("URL Selected: \(url)")
    }

    func didSelectTransitInformation(_ transitInformation: [String: String]) {
        print("TransitInformation Selected: \(transitInformation)")
    }

    func didSelectHashtag(_ hashtag: String) {
        print("Hashtag selected: \(hashtag)")
    }

    func didSelectMention(_ mention: String) {
        print("Mention selected: \(mention)")
    }

    func didSelectCustom(_ pattern: String, match: String?) {
        print("Custom data detector patter selected: \(pattern)")
    }

}

// MARK: - MessageInputBarDelegate

extension DialogViewController: InputBarAccessoryViewDelegate {

    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
    }

    private func insertMessages(_ dialogItem: DialogItemResponse) {
        let message = MessageItem(text: dialogItem.message.text,
                                  user: self.currentSender(),
                                  messageId: dialogItem.message.id,
                                  date: Date())
        insertMessage(message)
    }

    private func insertServerMessages(_ dialogItem: DialogItemResponse) {
        let message = MessageItem(text: dialogItem.message.text,
                                  user: self.serverAsSender(),
                                  messageId: dialogItem.message.id,
                                  date: Date())
        insertMessage(message)
    }
}


extension DialogViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return answerPickerData.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return answerPickerData[row].text
    }
}
