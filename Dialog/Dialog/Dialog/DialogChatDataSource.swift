//
//  DialogChatDataSource.swift
//  Dialog
//
//  Created by Artem Volkov on 07/12/2019.
//  Copyright © 2019 Artem Work. All rights reserved.
//

import Chatto
import ChattoAdditions

class DialogChatDataSource: ChatDataSourceProtocol {
    var hasMoreNext: Bool = false

    var hasMorePrevious: Bool = false

    var chatItems: [ChatItemProtocol] = []

    var delegate: ChatDataSourceDelegateProtocol?

    func loadNext() {
        // realise
    }

    func loadPrevious() {
        // realise
    }

    func adjustNumberOfMessages(preferredMaxCount: Int?,
                                focusPosition: Double,
                                completion: (Bool) -> Void)
    {
        // realise
    }



}
