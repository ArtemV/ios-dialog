//
//  UIColor+extensions.swift
//  Dialog
//
//  Created by Artem Volkov on 08/12/2019.
//  Copyright © 2019 Artem Work. All rights reserved.
//

import UIKit

public extension UIColor {

    static var primaryColor: UIColor {
        return #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
    }

}

