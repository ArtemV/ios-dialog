//
//  MessageItem.swift
//  Dialog
//
//  Created by Artem Volkov on 08/12/2019.
//  Copyright © 2019 Artem Work. All rights reserved.
//

import Foundation
import MessageKit
import InputBarAccessoryView


internal struct MessageItem: MessageType {

    var messageId: String
    var sender: SenderType {
        return user
    }
    var sentDate: Date
    var kind: MessageKind

    var user: SenderType

    private init(kind: MessageKind, user: SenderType, messageId: String, date: Date) {
        self.kind = kind
        self.user = user
        self.messageId = messageId
        self.sentDate = date
    }

//    init(custom: Any?, user: SenderType, messageId: String, date: Date) {
//        self.init(kind: .custom(custom), user: user, messageId: messageId, date: date)
//    }

    init(text: String, user: SenderType, messageId: String, date: Date) {
        self.init(kind: .text(text), user: user, messageId: messageId, date: date)
    }

//    init(attributedText: NSAttributedString, user: SenderType, messageId: String, date: Date) {
//        self.init(kind: .attributedText(attributedText), user: user, messageId: messageId, date: date)
//    }
//
//    init(image: UIImage, user: SenderType, messageId: String, date: Date) {
//        let mediaItem = ImageMediaItem(image: image)
//        self.init(kind: .photo(mediaItem), user: user, messageId: messageId, date: date)
//    }
}
